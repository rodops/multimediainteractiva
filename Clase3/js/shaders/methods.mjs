/* contexto de gl, vertexShader, fragmentShader */
export function initShaderProgram(gl, vsSource, fsSource) {
    /* Cargar shaders */
    const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
    const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, vsSource);

    const shaderProgram = gl.createProgram();

    try {
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
    
        gl.linkProgram(shaderProgram);

        if(!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            throw `No se inicializo el programa del Shader ${gl.getProgramInfoLog(shaderProgram)}`;
        }
    } catch(error) {
        console.error(error);

        return null;
    }

    return shaderProgram;
}

function loadShader(gl, type, source) {
    const shader = gl.createShader(type);

    try {
        /* Mandamos el CF a el objeto Shader */
        gl.shaderSource(shader, source);

        /* Compilar el programa del Shader */
        gl.compileShader(shader);

        if(!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            throw `Sucedio un error de compilacion: ${gl.getShaderInfoLog(shader)}`;
        }
    } catch(error) {
        console.error(error);

        gl.deleteShader(shader);

        return null;
    }

    return shader;
}